from keras.metrics import MeanIoU
from keras.optimizers import Adam, SGD
from utils.utils import *
from metrics.validation_metrics import *
from models.unet import build_unet
from models.deeplabv3 import build_deeplabv3
from models.segnet import build_segnet
from keras.layers import Input
from keras.regularizers import l2
from keras.utils import plot_model
from data_loader.data_generator import DataGenerator, get_data_indices, show_data_layout, generate_predictions, flatten_predictions
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from keras.callbacks import CSVLogger, ReduceLROnPlateau, EarlyStopping, ModelCheckpoint, LearningRateScheduler
from sklearn.preprocessing import OneHotEncoder


input_layer = Input(INPUT_SHAPE)
loss_cat_crossentropy = "categorical_crossentropy"
loss_bin_crossentropy = 'binary_crossentropy'
ker_init = "he_normal"
dropout = 0.2
kernel_regularizer = l2(1e-5)
adam_learning_rate = 0.001
adam_optimizer = Adam(learning_rate=adam_learning_rate)
metrics = ['accuracy', MeanIoU(
    num_classes=4), dice_coef, precision, sensitivity, specificity, dice_coef_necrotic, dice_coef_edema, dice_coef_enhancing]


# splt training data into training, testing and validation
train_ids, test_ids, val_ids = get_data_indices()

# print training data distribution
show_data_layout(train_ids, test_ids, val_ids)

# create data generators
training_generator = DataGenerator(train_ids)
test_generator = DataGenerator(test_ids)
validation_generator = DataGenerator(val_ids)

# create callbacks
# csv_logger = CSVLogger('training.log', separator=',', append=False)
callbacks = [
    EarlyStopping(patience=10, verbose=1),
    ModelCheckpoint('model_checkpoint.h5', save_best_only=True,
                    verbose=1)
    # , csv_logger
]

# U-Net
# build and plot model
unet_model = build_unet(input_layer, ker_init, dropout)
unet_model.compile(loss=loss_cat_crossentropy,
                   optimizer=adam_optimizer, metrics=metrics)
plot_model(unet_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/unet_architecture.png",
           dpi=70)

# trian model
# K.clear_session()
# unet_history = unet_model.fit(training_generator,
#                               epochs=50,
#                               steps_per_epoch=len(train_ids),
#                               callbacks=callbacks,
#                               validation_data=validation_generator
#                               )
# unet_model.save("unet_model.h5")

# DeepLabV3+
# build and plot model
deeplabv3_model = build_deeplabv3(input_layer, ker_init)
deeplabv3_model.compile(loss=loss_cat_crossentropy,
                        optimizer=adam_optimizer, metrics=metrics)
plot_model(deeplabv3_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/deeplabv3_architecture.png",
           dpi=70)

# train model
# K.clear_session()
# deeplabv3_history = deeplabv3_model.fit(training_generator,
#                                         epochs=50,
#                                         steps_per_epoch=len(train_ids),
#                                         callbacks=callbacks,
#                                         validation_data=validation_generator
#                                         )
# deeplabv3_model.save("deeplabv3_model.h5")


# SegNet
# build and plot model
segnet_model = build_segnet(input_layer, ker_init)
segnet_model.compile(loss=loss_cat_crossentropy,
                     optimizer=adam_optimizer, metrics=metrics)
plot_model(segnet_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/segnet_architecture.png",
           dpi=70)

# train model
# K.clear_session()
# segnet_history = segnet_model.fit(training_generator,
#                                   epochs=50,
#                                   steps_per_epoch=len(train_ids),
#                                   callbacks=callbacks,
#                                   validation_data=validation_generator
#                                   )
# segnet_model.save("segnet_model.h5")

# Ensemble 1
# RandomForest ensemble for the predictions of U-Net, DeepLabV3, SegNet
models = [unet_model, deeplabv3_model, segnet_model]
for model in models:
    model.fit(
        training_generator,
        epochs=50,
        batch_size=32,
        validation_data=validation_generator,
        callbacks=callbacks
    )

# Generate predictions for each model using the generators
unet_preds_train, unet_truths_train = generate_predictions(
    unet_model, training_generator)
deeplabv3_preds_train, _ = generate_predictions(
    deeplabv3_model, training_generator)
segnet_preds_train, _ = generate_predictions(segnet_model, training_generator)

unet_preds_val, unet_truths_val = generate_predictions(
    unet_model, validation_generator)
deeplabv3_preds_val, _ = generate_predictions(
    deeplabv3_model, validation_generator)
segnet_preds_val, _ = generate_predictions(segnet_model, validation_generator)

unet_preds_test, unet_truths_test = generate_predictions(
    unet_model, test_generator)
deeplabv3_preds_test, _ = generate_predictions(deeplabv3_model, test_generator)
segnet_preds_test, _ = generate_predictions(segnet_model, test_generator)

# Flatten the predictions for Random Forest input
unet_preds_train_flat, unet_truths_train_flat = flatten_predictions(
    unet_preds_train, unet_truths_train)
deeplabv3_preds_train_flat, _ = flatten_predictions(
    deeplabv3_preds_train, unet_truths_train)
segnet_preds_train_flat, _ = flatten_predictions(
    segnet_preds_train, unet_truths_train)

unet_preds_val_flat, unet_truths_val_flat = flatten_predictions(
    unet_preds_val, unet_truths_val)
deeplabv3_preds_val_flat, _ = flatten_predictions(
    deeplabv3_preds_val, unet_truths_val)
segnet_preds_val_flat, _ = flatten_predictions(
    segnet_preds_val, unet_truths_val)

unet_preds_test_flat, unet_truths_test_flat = flatten_predictions(
    unet_preds_test, unet_truths_test)
deeplabv3_preds_test_flat, _ = flatten_predictions(
    deeplabv3_preds_test, unet_truths_test)
segnet_preds_test_flat, _ = flatten_predictions(
    segnet_preds_test, unet_truths_test)

# Combine the predictions to form input features for the Random Forest
X_train = np.concatenate(
    [unet_preds_train_flat, deeplabv3_preds_train_flat, segnet_preds_train_flat], axis=-1)
y_train = np.argmax(unet_truths_train_flat, axis=-1)

X_val = np.concatenate(
    [unet_preds_val_flat, deeplabv3_preds_val_flat, segnet_preds_val_flat], axis=-1)
y_val = np.argmax(unet_truths_val_flat, axis=-1)

X_test = np.concatenate(
    [unet_preds_test_flat, deeplabv3_preds_test_flat, segnet_preds_test_flat], axis=-1)
y_test = np.argmax(unet_truths_test_flat, axis=-1)

# Train the Random Forest
rf = RandomForestClassifier(n_estimators=100, random_state=42)
rf.fit(X_train, y_train)

# Validate the Random Forest
val_preds_rf = rf.predict(X_val)
test_preds_rf = rf.predict(X_test)

# Convert the RF predictions to one-hot encoding
encoder = OneHotEncoder(sparse=False, categories='auto')
val_preds_rf_onehot = encoder.fit_transform(val_preds_rf.reshape(-1, 1))
test_preds_rf_onehot = encoder.fit_transform(test_preds_rf.reshape(-1, 1))

# Reshape back to the original shape
val_preds_rf_onehot = val_preds_rf_onehot.reshape(unet_truths_val.shape)
test_preds_rf_onehot = test_preds_rf_onehot.reshape(unet_truths_test.shape)

# Calculate Dice coefficient for validation set
val_dice_coef = dice_coef(unet_truths_val, val_preds_rf_onehot).numpy()
val_dice_coef_necrotic = dice_coef_necrotic(
    unet_truths_val, val_preds_rf_onehot).numpy()
val_dice_coef_edema = dice_coef_edema(
    unet_truths_val, val_preds_rf_onehot).numpy()
val_dice_coef_enhancing = dice_coef_enhancing(
    unet_truths_val, val_preds_rf_onehot).numpy()

print(f'Validation Dice Coefficient: {val_dice_coef}')
print(f'Validation Dice Coefficient (Necrotic): {val_dice_coef_necrotic}')
print(f'Validation Dice Coefficient (Edema): {val_dice_coef_edema}')
print(f'Validation Dice Coefficient (Enhancing): {val_dice_coef_enhancing}')

# Calculate Dice coefficient for test set
test_dice_coef = dice_coef(unet_truths_test, test_preds_rf_onehot).numpy()
test_dice_coef_necrotic = dice_coef_necrotic(
    unet_truths_test, test_preds_rf_onehot).numpy()
test_dice_coef_edema = dice_coef_edema(
    unet_truths_test, test_preds_rf_onehot).numpy()
test_dice_coef_enhancing = dice_coef_enhancing(
    unet_truths_test, test_preds_rf_onehot).numpy()

print(f'Test Dice Coefficient: {test_dice_coef}')
print(f'Test Dice Coefficient (Necrotic): {test_dice_coef_necrotic}')
print(f'Test Dice Coefficient (Edema): {test_dice_coef_edema}')
print(f'Test Dice Coefficient (Enhancing): {test_dice_coef_enhancing}')

# Calculate mean IOU for validation set
val_mean_iou = np.mean([np.intersect1d(
    y_val, val_preds_rf).size / np.union1d(y_val, val_preds_rf).size])
print(f'Validation Mean IoU: {val_mean_iou}')

# Calculate mean IOU for test set
test_mean_iou = np.mean([np.intersect1d(
    y_test, test_preds_rf).size / np.union1d(y_test, test_preds_rf).size])
print(f'Test Mean IoU: {test_mean_iou}')


# Ensemble 2
# Two U-Nets trained on different pipelines whose weights are averaged
# U-Net 1 training: batch size 8, Adam optimizer (lr=1e-4), validation performed each 3 epochs with early stopping, no weight decay.
unet_model_1 = build_unet(input_layer, ker_init, dropout)
unet_model_1.compile(optimizer=Adam(lr=1e-4),
                     loss=loss_cat_crossentropy, metrics=metrics)
callbacks_unet_model_1 = [
    EarlyStopping(monitor='val_loss', patience=3,
                  verbose=1, restore_best_weights=True)
]

unet_model_1_history = unet_model_1.fit(training_generator,
                                        epochs=100,
                                        batch_size=8,
                                        validation_data=validation_generator,
                                        callbacks=callbacks)
unet_model_1.save("unet_model_1.h5")

# U-Net 2 training: batch size 16, Stochastic Gradient Descent With Restarts (lr=1e-4 and cosine annealing), L2 regularisation.
unet_model_2 = build_unet(input_layer, ker_init, dropout, kernel_regularizer)
unet_model_2.compile(optimizer=SGD(lr=1e-4),
                     loss=loss_bin_crossentropy, metrics=metrics)
callbacks_unet_model_2 = [
    EarlyStopping(monitor='val_loss', patience=3,
                  verbose=1, restore_best_weights=True),
    LearningRateScheduler(lr_scheduler)
]

unet_model_2_history = unet_model_2.fit(training_generator,
                                        epochs=100,
                                        batch_size=16,
                                        validation_data=validation_generator,
                                        callbacks=callbacks)
unet_model_2.save("unet_model_2.h5")

# weights averaging
for layer1, layer2 in zip(unet_model_1.layers, unet_model_2.layers):
    if layer1.get_weights() and layer2.get_weights():
        layer1_weights = layer1.get_weights()
        layer2_weights = layer2.get_weights()
        averaged_weights = [(w1 + w2) / 2 for w1,
                            w2 in zip(layer1_weights, layer2_weights)]
        layer1.set_weights(averaged_weights)
unet_model_1.save("unet_model_averaged_weights.h5")

final_model = unet_model_1

# Generate predictions using the final model
final_preds_val, final_truths_val = generate_predictions(
    final_model, validation_generator)
final_preds_test, final_truths_test = generate_predictions(
    final_model, test_generator)

# Calculate Dice coefficient for validation set
val_dice_coef = dice_coef(final_truths_val, final_preds_val).numpy()
val_dice_coef_necrotic = dice_coef_necrotic(
    final_truths_val, final_preds_val).numpy()
val_dice_coef_edema = dice_coef_edema(
    final_truths_val, final_preds_val).numpy()
val_dice_coef_enhancing = dice_coef_enhancing(
    final_truths_val, final_preds_val).numpy()

print(f'Validation Dice Coefficient: {val_dice_coef}')
print(f'Validation Dice Coefficient (Necrotic): {val_dice_coef_necrotic}')
print(f'Validation Dice Coefficient (Edema): {val_dice_coef_edema}')
print(f'Validation Dice Coefficient (Enhancing): {val_dice_coef_enhancing}')

# Calculate Dice coefficient for test set
test_dice_coef = dice_coef(final_truths_test, final_preds_test).numpy()
test_dice_coef_necrotic = dice_coef_necrotic(
    final_truths_test, final_preds_test).numpy()
test_dice_coef_edema = dice_coef_edema(
    final_truths_test, final_preds_test).numpy()
test_dice_coef_enhancing = dice_coef_enhancing(
    final_truths_test, final_preds_test).numpy()

print(f'Test Dice Coefficient: {test_dice_coef}')
print(f'Test Dice Coefficient (Necrotic): {test_dice_coef_necrotic}')
print(f'Test Dice Coefficient (Edema): {test_dice_coef_edema}')
print(f'Test Dice Coefficient (Enhancing): {test_dice_coef_enhancing}')

# Calculate Mean IoU for validation set
mean_iou_metric = tf.keras.metrics.MeanIoU(num_classes=4)
mean_iou_metric.update_state(
    tf.argmax(final_truths_val, axis=-1), tf.argmax(final_preds_val, axis=-1))
val_mean_iou = mean_iou_metric.result().numpy()
print(f'Validation Mean IoU: {val_mean_iou}')

# Calculate Mean IoU for test set
mean_iou_metric = tf.keras.metrics.MeanIoU(num_classes=4)
mean_iou_metric.update_state(
    tf.argmax(final_truths_test, axis=-1), tf.argmax(final_preds_test, axis=-1))
test_mean_iou = mean_iou_metric.result().numpy()
print(f'Test Mean IoU: {test_mean_iou}')
