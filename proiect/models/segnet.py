from keras.layers import Conv2D, BatchNormalization, Activation, MaxPooling2D, UpSampling2D, Reshape
from keras.models import Model
from utils.utils import *
from custom_layers.custom_layers import MaxPoolingWithArgmax2D, MaxUnpooling2D


def build_segnet(input, ker_init):
    conv_kernel_shape=(3,3)
    pool_size=(2, 2)

    # encoder
    conv_1 = Conv2D(64, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(input)
    conv_1 = BatchNormalization()(conv_1)
    conv_1 = Activation("relu")(conv_1)
    conv_2 = Conv2D(64, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(conv_1)
    conv_2 = BatchNormalization()(conv_2)
    conv_2 = Activation("relu")(conv_2)

    pool_1, mask_1 = MaxPoolingWithArgmax2D(pool_size)(conv_2)

    conv_3 = Conv2D(128, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(pool_1)
    conv_3 = BatchNormalization()(conv_3)
    conv_3 = Activation("relu")(conv_3)
    conv_4 = Conv2D(128, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(conv_3)
    conv_4 = BatchNormalization()(conv_4)
    conv_4 = Activation("relu")(conv_4)

    pool_2, mask_2 = MaxPoolingWithArgmax2D(pool_size)(conv_4)

    conv_5 = Conv2D(256, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(pool_2)
    conv_5 = BatchNormalization()(conv_5)
    conv_5 = Activation("relu")(conv_5)
    conv_6 = Conv2D(256, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(conv_5)
    conv_6 = BatchNormalization()(conv_6)
    conv_6 = Activation("relu")(conv_6)
    conv_7 = Conv2D(256, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(conv_6)
    conv_7 = BatchNormalization()(conv_7)
    conv_7 = Activation("relu")(conv_7)

    pool_3, mask_3 = MaxPoolingWithArgmax2D(pool_size)(conv_7)

    conv_8 = Conv2D(512, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(pool_3)
    conv_8 = BatchNormalization()(conv_8)
    conv_8 = Activation("relu")(conv_8)
    conv_9 = Conv2D(512, conv_kernel_shape, padding="same",
                    kernel_initializer=ker_init)(conv_8)
    conv_9 = BatchNormalization()(conv_9)
    conv_9 = Activation("relu")(conv_9)
    conv_10 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_9)
    conv_10 = BatchNormalization()(conv_10)
    conv_10 = Activation("relu")(conv_10)

    pool_4, mask_4 = MaxPoolingWithArgmax2D(pool_size)(conv_10)

    conv_11 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(pool_4)
    conv_11 = BatchNormalization()(conv_11)
    conv_11 = Activation("relu")(conv_11)
    conv_12 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_11)
    conv_12 = BatchNormalization()(conv_12)
    conv_12 = Activation("relu")(conv_12)
    conv_13 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_12)
    conv_13 = BatchNormalization()(conv_13)
    conv_13 = Activation("relu")(conv_13)

    pool_5, mask_5 = MaxPoolingWithArgmax2D(pool_size)(conv_13)

    # decoder
    unpool_1 = MaxUnpooling2D(pool_size)([pool_5, mask_5])

    conv_14 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(unpool_1)
    conv_14 = BatchNormalization()(conv_14)
    conv_14 = Activation("relu")(conv_14)
    conv_15 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_14)
    conv_15 = BatchNormalization()(conv_15)
    conv_15 = Activation("relu")(conv_15)
    conv_16 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_15)
    conv_16 = BatchNormalization()(conv_16)
    conv_16 = Activation("relu")(conv_16)

    unpool_2 = MaxUnpooling2D(pool_size)([conv_16, mask_4])

    conv_17 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(unpool_2)
    conv_17 = BatchNormalization()(conv_17)
    conv_17 = Activation("relu")(conv_17)
    conv_18 = Conv2D(512, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_17)
    conv_18 = BatchNormalization()(conv_18)
    conv_18 = Activation("relu")(conv_18)
    conv_19 = Conv2D(256, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_18)
    conv_19 = BatchNormalization()(conv_19)
    conv_19 = Activation("relu")(conv_19)

    unpool_3 = MaxUnpooling2D(pool_size)([conv_19, mask_3])

    conv_20 = Conv2D(256, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(unpool_3)
    conv_20 = BatchNormalization()(conv_20)
    conv_20 = Activation("relu")(conv_20)
    conv_21 = Conv2D(256, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_20)
    conv_21 = BatchNormalization()(conv_21)
    conv_21 = Activation("relu")(conv_21)
    conv_22 = Conv2D(128, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_21)
    conv_22 = BatchNormalization()(conv_22)
    conv_22 = Activation("relu")(conv_22)

    unpool_4 = MaxUnpooling2D(pool_size)([conv_22, mask_2])

    conv_23 = Conv2D(128, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(unpool_4)
    conv_23 = BatchNormalization()(conv_23)
    conv_23 = Activation("relu")(conv_23)
    conv_24 = Conv2D(64, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(conv_23)
    conv_24 = BatchNormalization()(conv_24)
    conv_24 = Activation("relu")(conv_24)

    unpool_5 = MaxUnpooling2D(pool_size)([conv_24, mask_1])

    conv_25 = Conv2D(64, conv_kernel_shape, padding="same",
                     kernel_initializer=ker_init)(unpool_5)
    conv_25 = BatchNormalization()(conv_25)
    conv_25 = Activation("relu")(conv_25)

    conv_26 = Conv2D(NUM_CLASSES, (1, 1), padding="valid")(conv_25)
    conv_26 = BatchNormalization()(conv_26)
    conv_26 = Reshape(
        (INPUT_SHAPE[0] * INPUT_SHAPE[1], NUM_CLASSES),
        input_shape=(INPUT_SHAPE[0], INPUT_SHAPE[1], NUM_CLASSES),
    )(conv_26)

    outputs = Activation("softmax")(conv_26)

    return Model(inputs=input, outputs=outputs, name="SegNet")
