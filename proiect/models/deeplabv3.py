from keras.models import Model
from utils.utils import *
from keras.applications import MobileNetV2
from keras.layers import Conv2D, BatchNormalization, Concatenate, AveragePooling2D, UpSampling2D, Activation
import tensorflow as tf
import os
import keras

os.environ['KERAS_HOME'] = '/storage/sorinaau/.keras/'


def convolution_block(ker_init, block_input, num_filters=256, kernel_size=3, dilation_rate=1, use_bias=False):
    conv2d = Conv2D(num_filters, kernel_size=kernel_size, dilation_rate=dilation_rate,
                    padding="same", use_bias=use_bias, kernel_initializer=ker_init)(block_input)
    batch_norm = BatchNormalization()(conv2d)
    return Activation('relu')(batch_norm)


def dilated_spatial_pyramid_pooling(input, ker_init):
    dims = input.shape
    avg_pool_2d_1 = AveragePooling2D(pool_size=(dims[-3], dims[-2]))(input)
    conv_block_1 = convolution_block(
        ker_init, avg_pool_2d_1, kernel_size=1, use_bias=True)
    upsampling = UpSampling2D(size=(
        dims[-3] // conv_block_1.shape[1], dims[-2] // conv_block_1.shape[2]), interpolation="bilinear",)(conv_block_1)

    conv_block_2 = convolution_block(
        ker_init, input, kernel_size=1, dilation_rate=1)
    conv_block_3 = convolution_block(
        ker_init, input, kernel_size=3, dilation_rate=6)
    conv_block_4 = convolution_block(
        ker_init, input, kernel_size=3, dilation_rate=12)
    conv_block_5 = convolution_block(
        ker_init, input, kernel_size=3, dilation_rate=18)

    concat = Concatenate(
        axis=-1)([upsampling, conv_block_2, conv_block_3, conv_block_4, conv_block_5])
    conv_block_6 = convolution_block(ker_init, concat, kernel_size=1)
    return conv_block_6


def build_deeplabv3(input, ker_init):
    adjusted_inputs = Conv2D(3, (3, 3), padding='same', name='input_adjust')(input)
    adjusted_inputs = BatchNormalization()(adjusted_inputs)
    adjusted_inputs = Activation('relu')(adjusted_inputs)
    preprocessed_input = keras.applications.mobilenet_v2.preprocess_input(
        adjusted_inputs)
    mobilenet_v2 = MobileNetV2(include_top=False,
                               input_tensor=preprocessed_input, weights='imagenet')
    mobilenet_v2_output_1 = mobilenet_v2.get_layer(
        "block_13_expand_relu").output
    dspp1 = dilated_spatial_pyramid_pooling(mobilenet_v2_output_1, ker_init)
    dspp1_upsampled = UpSampling2D(size=(
        IMG_SIZE // 4 // dspp1.shape[1], IMG_SIZE // 4 // dspp1.shape[2]), interpolation="bilinear",)(dspp1)

    mobilenet_v2_output_2 = mobilenet_v2.get_layer(
        "block_3_expand_relu").output
    mobilenet_v2_output_2 = convolution_block(
        ker_init, mobilenet_v2_output_2, num_filters=48, kernel_size=1)

    concat = Concatenate(axis=-1)([dspp1_upsampled, mobilenet_v2_output_2])
    conv_block_1 = convolution_block(ker_init, concat)
    conv_block_2 = convolution_block(ker_init, conv_block_1)
    upsample = UpSampling2D(size=(IMG_SIZE // conv_block_2.shape[1], IMG_SIZE // conv_block_2.shape[2]),
                            interpolation="bilinear",)(conv_block_2)

    model_output = Conv2D(NUM_CLASSES, kernel_size=(1, 1),
                          padding="same")(upsample)

    return Model(inputs=input, outputs=model_output)
